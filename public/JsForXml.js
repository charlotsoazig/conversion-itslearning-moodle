// Fonction pour lire le fichier et extraire les données
async function readZip() {
    const fileInput = document.getElementById('fileInputZip');
    const file = fileInput.files[0];
    console.log(file);

    if (!file) {
        alert('Please select a file first!');
        return;
    }

    const reader = new zip.ZipReader(new zip.BlobReader(file));
    const entries = await reader.getEntries();
    fichierResultat="";
    for (const entry of entries) {
        if (entry.filename.endsWith('.xml') &&  !entry.filename.includes('imsmanifest')) {
            const text = await entry.getData(new zip.TextWriter());
            fichierResultat+=extractData(text);
            
        }
      
    }
  //  output.textContent = fichierResultat;
    saveToFile(fichierResultat);
    await reader.close();
}

function extractData(xml) {
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(xml, 'application/xml');

    // récupération la balise contenant la question itemBody
    const itemBody = xmlDoc.getElementsByTagName('itemBody')[0];

    if (!itemBody) {
        alert('itemBody element not found in the XML.');
        return;
    }
    //intitule de la question est dans la 1ère balise div de itemBody
    const htmlContent = itemBody.getElementsByTagName('div')[0].innerHTML;

    //récupère une chaîne de caractère avec une balise p contenu de la section <!CDATA
    if (!htmlContent) {
        alert('HTML content not found in itemBody.');
        return;
    }

    //récupération uniquement du text contenu dans la balise p
    console.log(htmlContent);
    console.log(htmlContent.length);
    indexEnd=htmlContent.length-7;
    question = htmlContent.substring(12, indexEnd);
    console.log(question);

    //récupération dans un tableau de la liste des bonnes réponses
    var tabCorrectAnswers = new Array();
    // le "numéro" de la bonne réponse est contenu dans une balise value
    correctAnswers = xmlDoc.getElementsByTagName('value');
    Array.from(correctAnswers).forEach((oneAnswer) => tabCorrectAnswers.push(oneAnswer.textContent));


    // Cas des choix multiples
    // récupération des réponses contenues dans des balises simpleChoices
    simpleChoices = itemBody.getElementsByTagName('simpleChoice');
    if (simpleChoices.length != 0) {

        typeReponse = "qcm";

    }

    // Cas des réponses courtes
    const isReponseCourte = itemBody.getElementsByTagName('extendedTextInteraction');
    if (isReponseCourte.length > 0) {
        simpleChoices = null;
        typeReponse = "repCourte";
    }

    //cas des mots manquants
    textAvecMotsManquants = itemBody.getElementsByTagName('p');
    if (textAvecMotsManquants.length > 0) {
        simpleChoices = textAvecMotsManquants;
        typeReponse = 'motsManquants';
        // le "numéro" de la bonne réponse est contenu dans une balise value
        correctAnswers = xmlDoc.getElementsByTagName('responseDeclaration');
        tabCorrectAnswers = correctAnswers;
    }


   return displayDataFormatGift(question, simpleChoices, tabCorrectAnswers, typeReponse);

}

// Fonction pour afficher les données extraites
function displayDataFormatGift(question, reponses, tabReponsesCorrectes, typeReponse) {
    const output = document.getElementById('output');
    const htmlParser = new DOMParser();
    question = question.replace(/=/g, '\\=');
    // Remplacer { par \{
    question = question.replace(/{/g, '\\{');
    // Remplacer } par \}
    question = question.replace(/}/g, '\\}');
    let resultatGift = question + ' {';
    if (typeReponse == "qcm") {
        Array.from(reponses).forEach((uneReponse) => {
            //récupération du contenu de la balise div (enfant de simpleCHoice) qui contient une balise CDATA dans laquelle il y a un p avec l'intitule de la question
            divUneReponse = uneReponse.getElementsByTagName('div')[0].textContent.trim();
            divUneReponseHTML = htmlParser.parseFromString(divUneReponse, 'text/html');
            textUneReponse = divUneReponseHTML.querySelector('p').textContent;
            
            isReponseCorrecte = false;
            if (tabReponsesCorrectes.length > 1) pourcentage = 100 / tabReponsesCorrectes.length;
            tabReponsesCorrectes.forEach((uneReponseCorrecte) => {
                
                if (uneReponse.getAttribute('identifier') == uneReponseCorrecte)
                    isReponseCorrecte = true

            })
            if (tabReponsesCorrectes.length > 1)
                if (isReponseCorrecte)
                    resultatGift += '~%' + pourcentage + '%' + textUneReponse + ' ';
                else
                    resultatGift += '~%-100%' + textUneReponse + ' ';

            else
                if (isReponseCorrecte)
                    resultatGift += '=' + textUneReponse + ' ';
                else
                    resultatGift += '~' + textUneReponse + ' ';
        });

    }
    if (typeReponse == 'repCourte')
        tabReponsesCorrectes.forEach((uneReponseCorrecte) => { resultatGift += '=' + uneReponseCorrecte + ' ' });


    resultatGift += '}';
    if (typeReponse == "motsManquants") {
        resultatGift = question;
        //reponses contient toutes les balises p avec le texte et les mots manquants identifiés par la balise textEntryInteraction

        /* Array.from(reponses).forEach((uneReponse) => {
             
             for(i=0; i<uneReponse.childElementCount;i++){
                 
                 if (uneReponse.children[i].tagName=="textEntryInteraction"){
                      let valeurReponse= uneReponse.children[i].getAttribute('responseIdentifier');  
                      Array.from(tabReponsesCorrectes).forEach((uneReponseCorrecte)=>{
                         if(uneReponseCorrecte.getAttribute('identifier')==valeurReponse){
                             textUneReponse=uneReponseCorrecte.getElementsByTagName('value')[0].textContent;
                             resultatGift+=" {="+textUneReponse+"} ";
                         }
                     })
                 }
                 else {
                     resultatGift+= uneReponse.children[i].textContent;
                     console.log(uneReponse.children[i].textContent);
                 }
             }
         })*/

    }
    console.log(resultatGift);
    return  resultatGift =resultatGift+ '\n\n';
    
}

// Fonction pour écrire le contenu dans un fichier texte
function saveToFile(content) {
    // Créer un objet Blob contenant le contenu texte
    const blob = new Blob([content], { type: 'text/plain' });

    // Créer un objet URL à partir du Blob
    const url = URL.createObjectURL(blob);

    // Créer un élément <a> pour le téléchargement du fichier
    const a = document.createElement('a');
    a.href = url;
    a.download = 'questions_and_answers.txt'; // Nom du fichier

    // Simuler un clic sur l'élément <a> pour démarrer le téléchargement
    a.click();

    // Libérer l'URL
    URL.revokeObjectURL(url);
}